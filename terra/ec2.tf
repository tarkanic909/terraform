resource "aws_instance" "control" {
  ami           = var.image
  instance_type = var.instance
  security_groups = ["${aws_security_group.allow_http_and_ssh.name}"]
  vpc_security_group_ids = [aws_security_group.allow_http_and_ssh.id]
  user_data     = file("post-install.sh")

  tags = {
    Name  = "control-plane"
    Owner = var.owner
  }

}

resource "aws_instance" "worker" {
  count           = 3
  ami             = var.image
  instance_type   = var.instance
  #private_ip      = "172.31.48.${count.index + 71}"
  user_data       = file("post-install.sh")
  security_groups = ["${aws_security_group.allow_http_and_ssh.name}"]
  vpc_security_group_ids = [aws_security_group.allow_http_and_ssh.id]

  tags = {
    Name  = "worker${count.index + 1}"
    Owner = var.owner
  }
}

